#!/usr/bin/env python
"""This is a test."""

import argparse
import cProfile
import logging
import os
import sys
import tempfile
import timeit
from pathlib import Path

import yaml
from semgrep.cli import cli as sgrep_cli

logging.basicConfig(filename="progress.log", level=logging.DEBUG)


def run_semgrep(rule_path, target):
    """Run semgrep like a cli as python so we can profile."""
    sys.argv = ["semgrep", "-q", "--timeout", 0, "-f", rule_path, target]
    return sgrep_cli()


def main():
    """Main function to call if called as a script."""
    parser = argparse.ArgumentParser(description="Profiler for a set of semgrep rules")
    parser.add_argument(
        "-t",
        "--target",
        required=True,
        help="Path to code that semgrep should analyze.",
    )
    parser.add_argument(
        "-r",
        "--rules-path",
        required=True,
        help="Path to directory tree or file with the semgrep rules to profile.",
    )
    parser.add_argument("-p", "--profile")
    args = parser.parse_args()

    if args.profile:
        rule = select_rule(args.profile)
        rule_path = save_rule_to_temp(rule)
        cProfile.run("run_semgrep('%s', '%s')" % (rule_path, args.target))
    else:
        rules = load_rules(args.rules_path)

        result = [
            time_rule(rule_key, rule, args.target) for (rule_key, rule) in rules.items()
        ]
        result.sort(key=lambda r: r["time"][0], reverse=True)
        with open("time_output.yml", "w") as file:
            file.write(yaml.dump(result))


def load_rules(rules_path):
    """Parse the rules from a file or a directory."""
    if os.path.isfile(rules_path):
        rules = [load_rule_file(rules_path)]
    elif os.path.isdir(rules_path):
        rules = [
            load_rule_file(Path(root, file))
            for root, _, files in os.walk(rules_path)
            for file in files
        ]
    else:
        logging.error("'%s' is not a file or a directory of rules", rules_path)
        sys.exit(1)

    return {
        "%s:%s" % (file, rule["id"]): rule
        for (file, file_rules) in rules
        for rule in file_rules
    }


def load_rule_file(rule_file):
    """Load the rules from a file."""
    with open(rule_file) as file:
        rules = yaml.safe_load(file)
        return (str(rule_file), rules["rules"])


def select_rule(profile_rule):
    """Select a single rule to profile."""
    path, profile_key = profile_rule.split(":")
    rules = load_rules(path)

    if profile_rule in rules:
        return rules[profile_rule]

    logging.error("'%s' is not a rule in %s", profile_key, path)
    sys.exit(2)


def save_rule_to_temp(rule):
    """Save a rule to a temporary file."""
    with tempfile.NamedTemporaryFile(delete=False) as file:
        file.write(bytes(yaml.dump({"rules": [rule]}), "utf-8"))
        return file.name


def time_rule(rule_key, rule, target):
    """Use timeit to time how long a rule takes to run."""
    logging.info("Timing: %s", rule_key)
    rule_path = save_rule_to_temp(rule)
    return {
        "time": timeit.repeat(
            "run_semgrep('%s', '%s')" % (rule_path, target),
            globals=globals(),
            number=1,
        ),
        "rule": rule_key,
    }


if __name__ == "__main__":
    main()
