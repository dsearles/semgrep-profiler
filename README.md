# Semgrep Profiler

usage: profile_semgrep.py [-h] -t TARGET -r RULES_PATH [-p PROFILE]

Profiler for a set of semgrep rules

optional arguments:
  -h, --help            show this help message and exit
  -t TARGET, --target TARGET
                        Path to code that semgrep should analyze.
  -r RULES_PATH, --rules-path RULES_PATH
                        Path to directory tree or file with the semgrep rules
                        to profile.
  -p PROFILE, --profile PROFILE

## Output

The result of timing rules will be written to time_output.yml in the directory where the script is ran.
